const calculaValor = require('../src/calcula-valor');

test('Subtrair dois valores - aula de git', () => {
    const subtracao = calculaValor.subtrair(2, 3);
    expect(subtracao).toBe(-1);
});

test('Teste de soma - aula de git', () => {
    const soma = calculaValor.somar(2, 3);
    expect(soma).toBe(5);
});
